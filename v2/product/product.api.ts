import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useRouter } from "next/router";
import { useEffect } from "react";

const collectionProduct = async (setting:ISetting) => 
{
	const result = await call(Types.COLLECTION_PRODUCT_SERVER, setting);
	
	return result;
}

const getProduct = async (setting:ISetting) => 
{
	const result = await call(Types.GET_PRODUCT_SERVER, setting);
	
	return result;
}

const getStockTableProduct = (setting:ISetting, onSuccess:any) => 
{
	const router = useRouter();

	useEffect(() => 
	{
		call(Types.GET_STOCK_TABLE_SERVER, setting).then(result => 
		{
			onSuccess(result.data)
		});
	}, [router.asPath]);	
}

const collection2Product = async (setting:ISetting) => 
{
	const result = await call(Types.COLLECTION_PRODUCT2_SERVER, setting);
	
	return result;
}

const getProduct2 = async (setting:ISetting) => 
{
	const result = await call(Types.GET_PRODUCT2_SERVER, setting);
	
	return result;
}

export { 
	collectionProduct, 
	getProduct, 
	collection2Product, 
	getProduct2,
	getStockTableProduct 
}