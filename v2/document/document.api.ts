import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const collectionDocument = async (setting:ISetting) => 
{
	const result = await call(Types.COLLECTION_DOCUMENT_SERVER, setting);
	
	return result;
}

const getDocument = async (setting:ISetting) => 
{
	const result = await call(Types.GET_DOCUMENT_SERVER, setting);
	
	return result;
}

const addDocument = async (setting:ISetting) => 
{
	const result = await call(Types.ADD_DOCUMENT_SERVER, setting);
	
	return result;
}

const setDocument = async (setting:ISetting) => 
{
	const result = await call(Types.SET_DOCUMENT_SERVER, setting);
	
	return result;
}

const incrementDocument = async (setting:ISetting) => 
{
	const result = await call(Types.INCREMENT_DOCUMENT_SERVER, setting);
	
	return result;
}

const cancelDocument = async (setting:ISetting) => 
{
	const result = await call(Types.DELETE_DOCUMENT_SERVER, setting);
	
	return result;
}

const setViews = async (setting:ISetting) => 
{
	const result = await call(Types.SET_VIEWS_SERVER, setting);
	
	return result;
}

export { collectionDocument, getDocument, addDocument, setViews, setDocument, cancelDocument, incrementDocument }