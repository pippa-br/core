import useSWR from 'swr';
import { call } from './call.api';

const options : any = {
	revalidateOnFocus  : true,
	revalidateIfStale  : true, // DEIXAR TRUE POR CONTA DO ONSUCCESS
	errorRetryCount    : 0,
	shouldRetryOnError : false,
};

const callSWR = (url, setting, onSuccess?:any) => 
{
	// PORQUE FAZEMOS ALTERAÇÕA DO OPTIONS COM O ONSUCCESS
	const optionsCall = Object.assign({}, options)

	if(onSuccess)
	{
		optionsCall.onSuccess = (result) => 
		{
			onSuccess(result.data);
		};
	}

	const { data, mutate, error } = useSWR(url, () => call(url, setting), optionsCall)

	if(error)
	{
		console.error(error);
	}

	return { 
		data   : data?.data, 		
		status : data?.status, 
		error  : data?.error,
		mutate : (value) => mutate(value),
	};
}

export { callSWR }