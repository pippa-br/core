import axios from "axios";
//import jwt from "jwt-simple";

//import fetch from 'node-fetch';

const api = axios.create();

const parseResponse = async (maps: any, result: any) => {
  if (result.data && maps.model) {
    result.data = new maps.model(result.data);
  } else if (result.collection && maps.collection) {
    result.collection = new maps.collection(result.collection);
  }

  return result;
};

const call = async (url: string, body?: any, req?: any, res?: any) => {
  try {
    let headers: any = {
      Accept: "application/json",
      "Content-Type": "application/json",
    };
    let response;
    let data;

    const token = '';

    // const token = jwt.encode(
    //   JSON.stringify(body),
    //   process.env.NEXT_PUBLIC_SECRET_KEY || ""
    // );

    if (req) {
      const cookie = req.headers.get("cookie") || {};
      headers.cookie = cookie;
      response = await fetch(url, {
        method: "POST",
        body: JSON.stringify(body),
        headers: headers,
      });
      data = await response.json();
    } else {
      response = await api.post(url, body, {
        headers: { "x-access-token": token },
      });
      data = response.data;
    }

    // SET HEADER AO RES
    if (res) {
      //res.headers.raw()['set-cookie'] = response.headers["set-cookie"];
      //res.setHeader("set-cookie", response.headers["set-cookie"]);
    }

    return data;
  } catch (e: any) {
    console.error(e);

    return {
      status: false,
    };
  }
};

const calls = async (...calls: any) => {
  let promises = [];

  for (const call of calls) {
    promises.push(call);
  }

  return await Promise.all(promises);
};

export { call, calls, parseResponse };
