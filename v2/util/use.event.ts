import { useRouter } from "next/router";
import { useEffect } from "react";

class UseEvents {
  static events = {};
  static dispatchs = {};
}

const clearEvent = () => {
  const router = useRouter();

  useEffect(() => {
    // console.log("clearEvent");

    UseEvents.events = {};
    UseEvents.dispatchs = {};
  }, [router.asPath]);
};

const dispatchEvent = (key: any, data?: any) => {
  UseEvents.dispatchs[key] = data;

  // console.log("dispatchEvent", key, UseEvents.events, UseEvents.dispatchs);

  if (UseEvents.events[key]) {
    for (const callback of UseEvents.events[key]) {
      callback(data);
    }
  }
};

const onEvent = (key: any, callback: any) => {
  const router = useRouter();

  useEffect(() => {
    // console.log("onEvent", key, UseEvents.events, UseEvents.dispatchs);

    if (!UseEvents.events[key]) {
      UseEvents.events[key] = [];
    }

    UseEvents.events[key].push(callback);

    if (UseEvents.dispatchs[key]) {
      callback(UseEvents.dispatchs[key]);
    }
  }, [router.asPath]);
};

export { clearEvent, dispatchEvent, onEvent };
