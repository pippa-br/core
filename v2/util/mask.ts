const dateMask = (date: any) => {
  return new Intl.DateTimeFormat("pt-BR").format(new Date(date));
};

const currencyMask = (number: any) => {
  return new Intl.NumberFormat("pt-BR", {
    // style: "currency",
    currency: "BRL",
  }).format(number);
  // return number
};

const cpfMask = (v: string) => {
  if (v.length >= 14) {
    return v.slice(0, 14);
  }
  v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
  v = v.replace(/(\d{3})(\d)/, "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
  v = v.replace(/(\d{3})(\d)/, "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
  //de novo (para o segundo bloco de números)
  v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2"); //Coloca um hífen entre o terceiro e o quarto dígitos
  return v;
};

const phoneMask = (phone: string) => {
  return phone
    .replace(/\D/g, "")
    .replace(/(\d{2})(\d)/, "($1) $2")
    .replace(/(\d{5})(\d{4})(\d)/, "$1-$2");
};

export { currencyMask, dateMask, phoneMask, cpfMask };
