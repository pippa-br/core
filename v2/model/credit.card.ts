import BaseModel from "./base.model";

export class CreditCard extends BaseModel
{
	populate(data)
	{
		this.expirydate    = data.creditCard.expirydate;
		this.referencePath = data.referencePath;
		this.owner		   = data.creditCard.owner;
		this.cardnumber    =  `**** **** **** ${data.creditCard.cardnumber.slice(data.creditCard.cardnumber.length - 4)}`;
	}
}