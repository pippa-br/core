const moment = require('moment-timezone');
moment.tz.setDefault("America/Sao_Paulo");
moment.locale('pt-BR');

export class TDate
{
	public date : any;

    constructor(value?:any, mask?:any, clearTime?:boolean)
    {
		// DATA SALVAS NAO PRECISA FAZER OFFSET DE HORA
		if(typeof value == 'string' || value instanceof Date)
		{
			this.date = moment(value, mask);
		}
		else if(!value)
		{
			let offsetHours = 0;

			this.date = moment().subtract({hours:offsetHours});
		}
		else if(value.seconds)
		{
			this.date = moment(value.seconds * 1000);
		}

		if(clearTime !== undefined && clearTime)
		{
			this.date.set({hour:0,minute:0,second:0,millisecond:0});
		}		
	}

	format(value?:string)
	{
		return this.date.format(value);
	}

	isValid()
	{
		return this.date.isValid();
	}

	add(value:any, format:any)
	{
		return this.date.add(value, format);
	}

	subtract(value:any, format:any)
	{
		return this.date.subtract(value, format);
	}

	diff(value:any, format:any)
	{
		return this.date.diff(value, format);
	}

	toDate()
	{
		return this.date.toDate();
	}

	month()
	{
		return this.date.month();
	}

	unix()
	{
		return this.date.unix();
	}
}