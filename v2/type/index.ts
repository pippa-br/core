export interface IType {
  	label: string;
  	value: string;
}

export default class Types 
{
  	// CONTAINER
  	static FLAT_CONTAINER    = { id: "flat", label: "Flat", value: "flat" };
  	static SEGMENT_CONTAINER = {
    	id    : "segment",
    	label : "Segment",
    	value : "segment",
  	};

	//MATRIX
	static COMBINE_MATRIX  = { value : 'combine',  label : 'Combinação', id : 0 };
	static SEPARATE_MATRIX = { value : 'separate', label : 'Separação',  id : 1 };
	static VARIANT_DEFAULT = [{ items : [{id : '_default',value : '_default'}]}];

  	// FIELD
  	static TEXT_FIELD  = { id: "text",  label: "Texto",  value: "text" };
  	static EMAIL_FIELD = { id: "email", label: "E-mail", value: "email" };

	// PAGARME
	static ADD_PAYMENT_PAGARME_SERVER = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/pagarmeApi/addPayment`;
	static ADD_CARD_PAGARME_SERVER    = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/pagarmeApi/addCard`;

	// FAVORITE
	static COLLECTION_FAVORITE_SERVER = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/favoriteApi/collection`;
	static GET_FAVORITE_SERVER 		  = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/favoriteApi/get`;
	static SET_FAVORITE_SERVER 		  = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/favoriteApi/set`;

  	// AUTH	  
	static GET_TOKEN_LOGIN_AUTH_SERVER = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/authV2/getTokenLogin`;
  	static LOGIN_AUTH_SERVER 	  	   = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/authV2/login`;
  	static LOGOUT_AUTH_SERVER 	  	   = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/authV2/logout`;
  	static GET_LOGGED_AUTH_SERVER 	   = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/authV2/getLogged`;
	static IS_LOGGED_AUTH_SERVER  	   = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/authV2/isLogged`;
	static VERIFY_LOGIN_AUTH_SERVER    = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/authV2/verifyLogin`;	

	// DOCUMENT
  	static COLLECTION_DOCUMENT_SERVER		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/documentV2/collection`;
  	static GET_DOCUMENT_SERVER 		 		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/documentV2/get`;
  	static GET_DOCUMENT_FRONT 		 		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/documentV2/get`;
  	static COLLECTION_PRODUCT_SERVER 		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/collectionProduct`;
  	static GET_PRODUCT_SERVER 		 		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/getProduct`; 		
  	static ADD_DOCUMENT_SERVER 		 		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/documentV2/add`;
	static ADD_REORDER_SERVER 		 	     = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/addReorder`;
  	static SET_DOCUMENT_SERVER		 		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/documentV2/set`;
	static INCREMENT_DOCUMENT_SERVER		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/documentV2/increment`;
	static SET_VIEWS_SERVER		 		 	 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/documentV2/setViews`;
  	static DELETE_DOCUMENT_SERVER 	 		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/documentV2/set`;
  	static UPLOAD_STORAGE_SERVER 	  		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/storageV2/upload2`;  
	static UPLOAD_MAKE_PUBLIC_STORAGE_SERVER = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/storageV2/uploadMakePublic`;  
	static ADD_USER_AUTH_SERVER 			 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/authV2/addUser`;
	static ADD_USER_WITH_TOKEN_SERVER		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/authV2/addUserWithToken`;
	static SET_USER_AUHT_SERVER 			 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/authV2/setUser`;
	static GET_USER_AUHT_SERVER 			 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/authV2/getUser`;
	static GET_ACCOUNT_SERVER 				 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/accountV2/get`;
	static RECOVERY_PASSWORD_SERVER 	 	 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/authV2/recoveryPassword`;	
	static CALCULATE_SHIPPING_SERVER   		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/shippingApi/calculateShippings`;
	static GET_TRACK_CORREIOS_SERVER   		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/correiosApi/track`;

	//ORDER
	static COLLECTION_ORDER_SERVER 		 	 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/orderApi/collection`;
  	static GET_ORDER_SERVER 		 		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/orderApi/get`;
	static SET_ITEM_ORDER_SERVER 		 	 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/orderApi/setItem`;
	static APPROVED_ORDER_SERVER 		 	 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/orderApi/approved`;
	static CANCELED_ORDER_SERVER 		 	 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/orderApi/canceled`;
	static SET_STATUS_BY_STORE_ORDER_SERVER  = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/orderApi/setSatusByStore`;
	static SPLIT_BY_STORE_ORDER_SERVER 		 = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/orderApi/splitByStore`;

	// PRODUCT 2
	static COLLECTION_PRODUCT2_SERVER = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/productApi/collection`;
  	static GET_PRODUCT2_SERVER 		  = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/productApi/get`; 	
	static GET_STOCK_TABLE_SERVER 	  = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/productApi/getStockTable`; 		  

	//CART
	static GET_CART_SERVER 			 		 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/getCart`; 
	static SET_ITEM_CART_SERVER 	 		 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setItemCart`;
	static SET_ITEMS_CART_SERVER 	 		 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setItemsCart`;
	static DEL_ITEM_CART_SERVER 	 		 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/delItemCart`;
	static DEL_ITEMS_BY_STORE_CART_SERVER 	 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/delItemsByStoreCart`;	
	static SET_SHIPPING_METHOD_CART_SERVER   	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setShippingCart`;
	static SET_PAYMENT_METHOD_CART_SERVER	 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setPaymentMethodCart`;
	static SET_CREDITCARD_REFERENCE_CART_SERVER = `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setCreditCardReferenceCart`;
	static SET_CREDITCARD_CART_SERVER 			= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setCreditCardCart`;
	static SET_ADDRESS_REFERENCE_CART_SERVER 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setAddressReferenceCart`;
	static SET_ADDRESS_CART_SERVER 				= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setAddressCart`;
	static SET_ATTACHMENT_CART_SERVER 		 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setAttachmentCart`;
	static SET_INSTALLMENT_CART_SERVER 	 	 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setInstallmentCart`;
	static SET_STORE_SERVER 	 	 			= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setStoreCart`;
	static CLEAR_CART_SERVER 	 	 		 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/clearCart`;	
	static CHECKOUT_CART_SERVER 			 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/checkoutCart`;
	static SET_COUPON_CART 			 			= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/setCouponCart`;
	static GET_CART_ITEMS_QUANTITY_SERVER	 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/countItemsCart`; 
	static VALIDATE_CART_SERVER 	 	 		= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/validateCart`;
	static GET_GLOBAL_CONTEXT_SERVER	 		= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/getGlobalContext`;
	static CALCULATE_ZIP_CODE_SERVER 		 	= `${process.env.NEXT_PUBLIC_API_SOURCE_URL}/ecomApi/calculateZipcodeCart`;
}
