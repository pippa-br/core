import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";
import { useRouter } from "next/router";

const collectionFavorite = async (setting:ISetting, onSuccess?:any) => 
{
	const router = useRouter();
	
	useEffect(() => 
	{
		call(Types.COLLECTION_FAVORITE_SERVER, setting).then((result) => 
		{
			onSuccess(result.collection || [])
		});	
	}, [router.asPath]);
}

const getFavorite = async (setting:ISetting, onSuccess?:any) => 
{
	const router = useRouter();
	
	useEffect(() => 
	{
		call(Types.GET_FAVORITE_SERVER, setting).then((result) => 
		{
			onSuccess(result.status)
		});	
	}, [router.asPath]);
}

const setFavorite = async (setting:ISetting) => 
{
	const result = await call(Types.SET_FAVORITE_SERVER, setting);
	
	return result;
}

export { collectionFavorite, getFavorite, setFavorite }