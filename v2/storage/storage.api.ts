import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const uploadStorage = async (setting:ISetting) => 
{
	const result = await call(Types.UPLOAD_STORAGE_SERVER, setting);
	
	return result;	
}

export { uploadStorage }