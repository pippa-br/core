import * as React from "react";
// import Factory from "../factory";
import mobiscroll from "@mobiscroll/react-lite";
import Formsy from "formsy-react";

// PIPPA
import Form from "../model/form";
import Types from "../type";

export interface IProps {
  form: Form;
}

export interface IState {}

class DynamicForm extends React.Component<IProps, IState> {
  constructor(props: any) {
    super(props);

    this.onInvalidSubmit = this.onInvalidSubmit.bind(this);
    this.onInvalidSubmit = this.onInvalidSubmit.bind(this);
    this.state = { canSubmit: false };
  }

  onInvalidSubmit(event: any) {
    //this.setState({ canSubmit : true });

    // console.log("----- onInvalidSubmit", event);
  }

  onValidSubmit(model: any) {
    // console.log("-----", model);
  }

  public render(): JSX.Element {
    if (this.props.form) {
      return (
        <Formsy
          onValidSubmit={this.onValidSubmit}
          onInvalidSubmit={this.onInvalidSubmit}
        >
          <mobiscroll.Form
            inputStyle="box"
            className="dynamic-form"
            lang="pt-BR"
          >
            <div className="mbsc-grid mbsc-grid-fixed">
              {this.displayContainer()}
              <div className="mbsc-row">
                <div className="mbsc-col">
                  <div className="mbsc-btn-group-block">
                    <mobiscroll.Button type="submit">
                      Adicionar
                    </mobiscroll.Button>
                  </div>
                </div>
              </div>
            </div>
          </mobiscroll.Form>
        </Formsy>
      );
    } else {
      return <div></div>;
    }
  }

  public displayContainer() {
    // const Component = Factory.getFormContainer(this.props.form);
    // return <Component form={this.props.form} />;
  }
}

export default DynamicForm;
