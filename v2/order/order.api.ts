import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const collectionOrder = async (setting: ISetting) => {
  const result = await call(Types.COLLECTION_ORDER_SERVER, setting);

  return result;
};

const getOrder = async (setting: ISetting) => {
  const result = await call(Types.GET_ORDER_SERVER, setting);

  return result;
};

const setStatusByStoreOrder = async (setting: ISetting) => {
  const result = await call(Types.SET_STATUS_BY_STORE_ORDER_SERVER, setting);

  return result;
};

const splitByStoreOrder = async (setting: ISetting) => {
  const result = await call(Types.SPLIT_BY_STORE_ORDER_SERVER, setting);

  return result;
};

const setItemOrder = async (setting: ISetting) => 
{
    const result = await call(Types.SET_ITEM_ORDER_SERVER, setting);

    return result;
};

const approvedOrder = async (setting: ISetting) => 
{
    const result = await call(Types.APPROVED_ORDER_SERVER, setting);

    return result;
};

const canceledOrder = async (setting: ISetting) => 
{
    const result = await call(Types.CANCELED_ORDER_SERVER, setting);

    return result;
};

export { collectionOrder, getOrder, setStatusByStoreOrder, splitByStoreOrder, setItemOrder, approvedOrder, canceledOrder };
