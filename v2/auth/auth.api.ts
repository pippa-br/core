import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";
import { useRouter } from "next/router";

const getLoggedAuth = (setting:ISetting, onSuccess:any) => 
{
	const router = useRouter();

	useEffect(() => 
	{
		call(Types.GET_LOGGED_AUTH_SERVER, setting).then(result => 
		{
			onSuccess(result.data)
		});
	}, [router.asPath]);	
}

const getUserAuth = (setting:ISetting, onSuccess:any) => 
{
	const router = useRouter();

	useEffect(() => 
	{
		call(Types.GET_USER_AUHT_SERVER, setting).then(result => 
		{
			onSuccess(result.data)
		})
	}, [router.asPath]);	
}

const verifyLoginAuth = async (setting:ISetting, req?:any, res?:any) => 
{
	const result = await call(Types.VERIFY_LOGIN_AUTH_SERVER, setting, req, res);
	
	return result;
}

const isLoggedAuth = async (setting:ISetting, req?:any, res?:any) => 
{
	const result = await call(Types.IS_LOGGED_AUTH_SERVER, setting, req, res);
	
	return result;
}

const addUserAuth = async (setting:ISetting) => 
{
	const result = await call(Types.ADD_USER_AUTH_SERVER, setting);
	
	return result;
}

const setUserAuth = async (setting:ISetting) => 
{
	const result = await call(Types.SET_USER_AUHT_SERVER, setting);
	
	return result;
}

const setUserAddress = async (setting:ISetting) => 
{
	const result = await call(Types.ADD_DOCUMENT_SERVER, setting);
	
	return result;
}

const setUserCreditCard = async (setting:ISetting) => 
{
	const result = await call(Types.ADD_DOCUMENT_SERVER, setting);
	
	return result;
}

const loginAuth = async (setting:ISetting) => 
{
	const result = await call(Types.LOGIN_AUTH_SERVER, setting);
	
	return result;
}

const logoutAuth = async (setting:ISetting) => 
{
	const result = await call(Types.LOGOUT_AUTH_SERVER, setting);
	
	return result;
}

const loginTokenAuth = async (setting:any) => 
{
	const result = await call(Types.ADD_USER_WITH_TOKEN_SERVER, setting);
	
	return result;
}

const getTokenLoginAuth = async (setting:any) => 
{
	const result = await call(Types.GET_TOKEN_LOGIN_AUTH_SERVER, setting);
	
	return result;
}

const recoveryPasswordAuth = async (setting:ISetting) => 
{
	const result = await call(Types.RECOVERY_PASSWORD_SERVER, setting);
	
	return result;
}

export { loginAuth, logoutAuth, recoveryPasswordAuth, getTokenLoginAuth, loginTokenAuth, verifyLoginAuth,
		 setUserAuth, setUserAddress, setUserCreditCard, addUserAuth, getUserAuth, getLoggedAuth, isLoggedAuth }