export default interface IUser {
  name: string;
  referencePath: string;
}

export interface UserProps {
  name: string;
  store: {
    name: string;
    code: string;
    id: string;
    referencePath: string;
    appid: string;
    accid: string;
  };
  type: string;
  cpf: string;
  cnpj: string;
  referencePath: string;
}
