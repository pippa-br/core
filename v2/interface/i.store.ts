export interface StoreProps {
    name: string;
    id: string;
    appid: string;
    accid: string;
    code: string;       
    minTotalItemsCart: number;
    slug: string;
    videos: [];
    minQuantityItemsCart: number;
    referencePath: string;
    banners: {
        image: {
        name: string;
        _url: string;
        id: string;
        colid: string;
        type: string;
        }
    }[];
    phone: string;
    description: string;
    priceViewer: {
        value: string;
        id: string;
        label: string;
    };
    logo: {
        image: {
            name: string;
            type: string;
            id: string;
            colid: string;
            _url: string;
        }
    };
    miniBanners: {
        image: {
            name: string;
            id: string;
            type: string;
            colid: string;
            _url: string;
        }
    }[],        
    autoApproval: boolean;
    allowCPF: boolean;
}


  