export interface BannerProps {
    desktop: {               
        _300x300: string;
        name: string;
        _150x150: string;
        _url: string;
        id: string;
        _1024x1024: string;
    },
    url: string;
    id: string;
    target: {
        value: string;
        label: string;
        id: string;
    },
    mobile: {
        _300x300: string;
        name: string;
        _150x150: string;
        _url: string;
        id: string;
        _1024x1024: string;
    }
}

