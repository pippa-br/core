import TagManager from "react-gtm-module";

export const tagManager = {
  initialize: (gtm) => {
    const tagManagerArgs = {
      gtmId: gtm.key,
    };

    TagManager.initialize(tagManagerArgs);
  },
  addToCart: (pagePath, product, quantity) => {
    TagManager.dataLayer({
      dataLayer: {
        event: "add_to_cart",
        pagePath: pagePath,
        pageTitle: "Produto",
        ecommerce: {
          items: tagManager.parse(product, quantity),
        },
        facebook_ecommerce: {
          items: [tagManager.facebookParse(product, quantity)],
          name: product.name,
          currency: "BRL",
          value: product.salePrice
            ? product.salePrice * quantity
            : product.price * quantity,
        },
      },
    });
  },
  removeFromCart: (pagePath, product, quantity) => {
    TagManager.dataLayer({
      dataLayer: {
        event: "remove_from_cart",
        pagePath: pagePath,
        pageTitle: "Produto",
        ecommerce: {
          items: tagManager.parse(product, quantity),
        },
      },
    });
  },
  addProduct: (pagePath, product) => {
    TagManager.dataLayer({
      dataLayer: {
        event: "view_item",
        pagePath: pagePath,
        pageTitle: "Produto",
        ecommerce: {
          items: tagManager.parse(product),
        },
        facebook_ecommerce: {
          items: [tagManager.facebookParse(product)],
        },
      },
    });
  },
  addProducts: (pagePath, products) => {
    let gTagproducts = products.map((product) => {
      return tagManager.parse(product);
    });
    let facebookProducts = products.map((product) => {
      return tagManager.facebookParse(product);
    });
    TagManager.dataLayer({
      dataLayer: {
        event: "view_item_list",
        pagePath: pagePath,
        pageTitle: "Produtos",
        ecommerce: {
          items: gTagproducts,
        },
        facebook_ecommerce: {
          category: pagePath,
          items: facebookProducts,
        },
      },
    });
  },
  checkout: (pagePath, products) => {
    let gtagProducts = products.map((product) => {
      return tagManager.parse(product);
    });
    let subtotal = 0;
    let productAmount = 0;
    let facebookProducts = products.map((product) => {
      productAmount += product.quantity;
      subtotal += product.salePrice
        ? product.salePrice * product.quantity
        : product.price * product.quantity;
      return tagManager.facebookParse(product);
    });
    TagManager.dataLayer({
      dataLayer: {
        event: "begin_checkout",
        pagePath: pagePath,
        pageTitle: "Checkout",
        ecommerce: {
          items: gtagProducts,
        },
        facebook_ecommerce: {
          items: facebookProducts,
          currency: "BRL",
          quantity: productAmount,
          value: subtotal,
        },
      },
    });
  },
  purchase: (pagePath, order) => {
    let gtagProducts = order.items.map((product) => {
      return tagManager.parse(product);
    });
    let facebookProducts = order.items.map((product) => {
      return tagManager.facebookParse(product);
    });
    TagManager.dataLayer({
      dataLayer: {
        event: "purchase",
        pagePath: pagePath,
        pageTitle: "Obrigado",
        ecommerce: {
          transaction_id: order.id,
          affiliation: "Opticas Urbano",
          value: order.total,
          tax: 0,
          shipping: order.totalShipping,
          currency: "BRL",
          // coupon: "SUMMER_SALE",
          items: gtagProducts,
        },
        facebook_ecommerce: {
          items: facebookProducts,
          currency: "BRL",
          value: order.total,
        },
      },
    });
  },
  parse: (product, quantity = null) => {
    let item = {
      item_name: product.name ? product.name : product.product.name,
      item_id: product.id,
      price: product.salePrice ? product.salePrice : product.price,
      item_category: product.categories ? product.categories[0].name : "",
      item_list_name: "Produtos",
      quantity: quantity ? quantity : product.quantity,
    };
    return item;
  },
  facebookParse: (product, quantity = null) => {
    let item = {
      id: product.id,
      quantity: quantity ? quantity : product.quantity,
    };
    return item;
  },
};
